/*

1. What built-in JS keyword is used to import packages?
require()

2. What Node.js module/package contains a method for server creation?
http module

3. What is the method of the http object responsible for creating a server using Node.js?

http.createServer((req,res) =>{
	
});

4. Between server and client, which creates a request?

request object

5. Between server and client, which creates a response?

response object

6. What is a runtime environment used to create stand-alone backend applications written in Javascript?

Node.js

7.What is the largest registry for Node packages?

npm
*/